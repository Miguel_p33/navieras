class BlMaster < ApplicationRecord
  belongs_to :naviera
  belongs_to :nvocc
  has_many :containers
  has_many :bl_houses
end
