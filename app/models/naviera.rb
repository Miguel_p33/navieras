class Naviera < ApplicationRecord
  has_many :containers
  has_and_belongs_to_many :nvoccs
  has_many :bl_masters
  has_many :vessels
end
