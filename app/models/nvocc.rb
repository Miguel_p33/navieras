class Nvocc < ApplicationRecord
  has_and_belongs_to_many :navieras
  has_many :bl_masters
  has_and_belongs_to_many :customers
end
