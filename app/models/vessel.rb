class Vessel < ApplicationRecord
  belongs_to :naviera
  has_many :containers
end
