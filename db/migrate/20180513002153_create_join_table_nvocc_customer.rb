class CreateJoinTableNvoccCustomer < ActiveRecord::Migration[5.2]
  def change
    create_join_table :nvoccs, :customers do |t|
      t.index [:nvocc_id, :customer_id]
      t.index [:customer_id, :nvocc_id]
    end
  end
end
