class CreateBlMasters < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_masters do |t|
      t.string :consigned
      t.string :list_containers
      t.string :origin_commodity
      t.string :destination_commodity
      t.date :date_berth
      t.integer :code_travel

      t.timestamps
    end
  end
end
