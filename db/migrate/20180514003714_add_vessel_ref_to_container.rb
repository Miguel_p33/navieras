class AddVesselRefToContainer < ActiveRecord::Migration[5.2]
  def change
    add_reference :containers, :vessel, foreign_key: true
  end
end
