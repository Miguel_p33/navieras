class AddBlMasterRefToContainer < ActiveRecord::Migration[5.2]
  def change
    add_reference :containers, :bl_master, foreign_key: true
  end
end
