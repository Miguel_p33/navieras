class CreateJoinTableNavieraNvocc < ActiveRecord::Migration[5.2]
  def change
    create_join_table :navieras, :nvoccs do |t|
      # t.index [:naviera_id, :nvocc_id]
      # t.index [:nvocc_id, :naviera_id]
    end
  end
end
