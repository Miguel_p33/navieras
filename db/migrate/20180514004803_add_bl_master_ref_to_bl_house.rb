class AddBlMasterRefToBlHouse < ActiveRecord::Migration[5.2]
  def change
    add_reference :bl_houses, :bl_master, foreign_key: true
  end
end
