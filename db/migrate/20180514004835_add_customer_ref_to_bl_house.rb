class AddCustomerRefToBlHouse < ActiveRecord::Migration[5.2]
  def change
    add_reference :bl_houses, :customer, foreign_key: true
  end
end
