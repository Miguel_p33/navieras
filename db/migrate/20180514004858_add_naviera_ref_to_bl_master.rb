class AddNavieraRefToBlMaster < ActiveRecord::Migration[5.2]
  def change
    add_reference :bl_masters, :naviera, foreign_key: true
  end
end
