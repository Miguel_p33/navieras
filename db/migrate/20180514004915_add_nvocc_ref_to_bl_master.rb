class AddNvoccRefToBlMaster < ActiveRecord::Migration[5.2]
  def change
    add_reference :bl_masters, :nvocc, foreign_key: true
  end
end
