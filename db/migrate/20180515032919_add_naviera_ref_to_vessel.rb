class AddNavieraRefToVessel < ActiveRecord::Migration[5.2]
  def change
    add_reference :vessels, :naviera, foreign_key: true
  end
end
