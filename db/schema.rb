# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_05_15_032919) do

  create_table "bl_houses", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "bl_master_id"
    t.integer "customer_id"
    t.index ["bl_master_id"], name: "index_bl_houses_on_bl_master_id"
    t.index ["customer_id"], name: "index_bl_houses_on_customer_id"
  end

  create_table "bl_masters", force: :cascade do |t|
    t.string "consigned"
    t.string "list_containers"
    t.string "origin_commodity"
    t.string "destination_commodity"
    t.date "date_berth"
    t.integer "code_travel"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "naviera_id"
    t.integer "nvocc_id"
    t.index ["naviera_id"], name: "index_bl_masters_on_naviera_id"
    t.index ["nvocc_id"], name: "index_bl_masters_on_nvocc_id"
  end

  create_table "containers", force: :cascade do |t|
    t.string "tipo"
    t.integer "size"
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "vessel_id"
    t.integer "bl_master_id"
    t.index ["bl_master_id"], name: "index_containers_on_bl_master_id"
    t.index ["vessel_id"], name: "index_containers_on_vessel_id"
  end

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customers_nvoccs", id: false, force: :cascade do |t|
    t.integer "nvocc_id", null: false
    t.integer "customer_id", null: false
    t.index ["customer_id", "nvocc_id"], name: "index_customers_nvoccs_on_customer_id_and_nvocc_id"
    t.index ["nvocc_id", "customer_id"], name: "index_customers_nvoccs_on_nvocc_id_and_customer_id"
  end

  create_table "navieras", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "navieras_nvoccs", id: false, force: :cascade do |t|
    t.integer "naviera_id", null: false
    t.integer "nvocc_id", null: false
  end

  create_table "nvoccs", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vessels", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "naviera_id"
    t.index ["naviera_id"], name: "index_vessels_on_naviera_id"
  end

end
