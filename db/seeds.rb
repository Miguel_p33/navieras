# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Naviera.create name:"costa VE, C.A"
Naviera.create name:"global ship, C.A"
Naviera.create name:"navieras paraguana, C.A"

Nvocc.create name:"expeditors"
Nvocc.create name:"cristal line"
Nvocc.create name:"apex"

Customer.create name:"miguel"
Customer.create name:"gabriela"
Customer.create name:"victor"
Customer.create name:"tefhy"

Vessel.create ([name:"bulk carrier", naviera_id: 1])
Vessel.create ([name:"general ship", naviera_id: 2])
Vessel.create ([name:"passenger ship", naviera_id: 3])
Vessel.create ([name:"general carrier", naviera_id: 2])

BlMaster.create origin_commodity: "venezuela", destination_commodity: "mexico", naviera_id: 1, nvocc_id: 1
BlMaster.create origin_commodity: "venezuela", destination_commodity: "colombia", naviera_id: 2, nvocc_id: 2
BlMaster.create origin_commodity: "venezuela", destination_commodity: "ecuador", naviera_id: 3, nvocc_id: 3
BlMaster.create origin_commodity: "argentina", destination_commodity: "mexico", naviera_id: 1, nvocc_id: 1
BlMaster.create origin_commodity: "ecuador", destination_commodity: "argentina", naviera_id: 2, nvocc_id: 2
BlMaster.create origin_commodity: "ecuador", destination_commodity: "uruguay", naviera_id: 3, nvocc_id: 3
BlMaster.create origin_commodity: "colombia", destination_commodity: "venezuela", naviera_id: 1, nvocc_id: 1

Container.create tipo:"Standar", size: 100 ,code:"AAAA1234567", vessel_id: 1, bl_master_id: 1
Container.create tipo:"High-cube", size: 200 ,code:"BBBB1234567", vessel_id: 2, bl_master_id: 2
Container.create tipo:"Standar", size: 300, code:"CCCC1234567", vessel_id: 3, bl_master_id: 3
Container.create tipo:"High-cube", size: 400, code:"DDDD1234567", vessel_id: 4 , bl_master_id: 4
Container.create tipo:"High-cube", size: 500, code:"EEEE1234567", vessel_id: 3, bl_master_id: 5

BlHouse.create bl_master_id: 1, customer_id: 1
BlHouse.create bl_master_id: 2, customer_id: 2
BlHouse.create bl_master_id: 3, customer_id: 3
BlHouse.create bl_master_id: 4, customer_id: 4
BlHouse.create bl_master_id: 5, customer_id: 1
BlHouse.create bl_master_id: 6, customer_id: 2
BlHouse.create bl_master_id: 7, customer_id: 3
BlHouse.create bl_master_id: 5, customer_id: 4

#table of naviera_nvocc
n = Naviera.find(1)
m = Nvocc.find(1)
n.nvoccs << m
n.save

n = Naviera.find(1)
m = Nvocc.find(2)
n.nvoccs << m
n.save

n = Naviera.find(1)
m = Nvocc.find(3)
n.nvoccs << m
n.save

n = Naviera.find(2)
m = Nvocc.find(1)
n.nvoccs << m
n.save

n = Naviera.find(2)
m = Nvocc.find(2)
n.nvoccs << m
n.save

n = Naviera.find(3)
m = Nvocc.find(2)
n.nvoccs << m
n.save

n = Naviera.find(3)
m = Nvocc.find(3)
n.nvoccs << m
n.save

#table of nvocc_customer
n= Nvocc.find(1)
c = Customer.find(1)
n.save

n= Nvocc.find(1)
c = Customer.find(2)
n.save

n= Nvocc.find(1)
c = Customer.find(3)
n.save

n= Nvocc.find(1)
c = Customer.find(4)
n.save

n= Nvocc.find(2)
c = Customer.find(1)
n.save

n= Nvocc.find(2)
c = Customer.find(3)
n.save

n= Nvocc.find(3)
c = Customer.find(2)
n.save

n= Nvocc.find(3)
c = Customer.find(4)
n.save
